#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from anytree import Node, RenderTree
from anytree.search import find


# Load orbits
file_obj = open("input", 'r')
orbits = [i.rstrip('\n').split(')') for i in file_obj.readlines()]
file_obj.close()

# Classify orbits by parent
orbits_dict = {}
for parent, children in orbits:
    if parent not in orbits_dict:
        orbits_dict[parent] = []
    orbits_dict[parent].append(children)

# Find childrens of parents
def find_childrens(parent):
    try:
        for child in orbits_dict[parent.name]:
            find_childrens(Node(child, parent=parent))
    except:
        pass

# Calculate orbits
def get_directly_orbits(parent, count=None):
    if not count:
        count = 0
    for children in parent.children:
        count = get_directly_orbits(children, count+1)

    return count

def get_indirectly_orbits(parent, count=None):
    if not count:
        count = 0
    for children in parent.children:
        count = get_indirectly_orbits(children, count+children.depth-1)

    return count

def get_common_path(node1, node2):
    for i in node1.ancestors[::-1]:
        for j in node2.ancestors[::-1]:
            if i == j:
                return i


root = Node('COM')
find_childrens(root)

# Part 1
# Result: 273985

print("Part 1:", get_indirectly_orbits(root) + get_directly_orbits(root))


# Part 2
# Result: 460
path_you = find(root, lambda node: node.name == "YOU")
path_san = find(root, lambda node: node.name == "SAN")

parent_common = get_common_path(path_you, path_san)
nodes_distance = path_you.depth + path_san.depth - 2*parent_common.depth - 2

print("Part 2:", nodes_distance)
