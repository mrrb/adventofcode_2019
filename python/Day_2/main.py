#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def load_memory():
    file_obj = open("input", 'r')
    memory = {}
    for index, value in enumerate(file_obj.read().split(',')):
        memory[index] = int(value)
    file_obj.close()
    return memory

def exec_op_add(memory, in1_pos, in2_pos, out_pos):
    memory[out_pos] = memory[in1_pos] + memory[in2_pos]

def exec_op_mul(memory, in1_pos, in2_pos, out_pos):
    memory[out_pos] = memory[in1_pos] * memory[in2_pos]

def exec_program(memory):
    pc = 0
    while True:
        op_code = memory[pc]
        if op_code == 1:
            exec_op_add(memory, memory[pc+1], memory[pc+2], memory[pc+3])
            pc += 4
        elif op_code == 2:
            exec_op_mul(memory, memory[pc+1], memory[pc+2], memory[pc+3])
            pc += 4
        elif op_code == 99:
            break
        else:
            raise BaseException(f"Invalid OpCode {op_code} at position {pc}.")

# Part 1
# Result: 3101878
memory1 = load_memory()
memory1[1], memory1[2] = 12, 2

exec_program(memory1)

print("Part 1:", memory1[0])

# Part 2
# Result: 8444
wanted_result = 19690720

def calculate_values():
    for noun in range(100):
        for verb in range(100):
            memory2 = load_memory()
            memory2[1], memory2[2] = noun, verb

            exec_program(memory2)
            if memory2[0] == wanted_result:
                return (noun, verb)

(noun, verb) = calculate_values()
result = 100*noun + verb

print("Part 2:", result)
