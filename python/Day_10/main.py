#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from math import sqrt


file_obj = open('input_test1', 'r')
data = [i.rstrip('\n') for i in file_obj.readlines()]
file_obj.close()

asteroid_map = {}
for n_line, line in enumerate(data):
    line_data = {}
    for n_column, column in enumerate(line):
        line_data[n_column] = {
            'type': 'asteroid' if column == "#" else 'empty'}
    asteroid_map[n_line] = line_data

asteroids = []
for k_line, line in asteroid_map.items():
    for k_column, column in line.items():
        if column['type'] == "asteroid":
            asteroids.append({'pos': [k_line, k_column]})

# print(asteroids)

for n_asteroid, asteroid in enumerate(asteroids):
    for asteroid2 in asteroids[:n_asteroid] + asteroids[n_asteroid+1:]:
        dis = [asteroid2['pos'][0]-asteroid['pos'][0],
               asteroid2['pos'][1]-asteroid['pos'][1]]
        # dis.append(sqrt(pow(dis[0], 2) + pow(dis[1], 2)))
        # print(asteroid, ">>", asteroid2, ">>", dis, ">>" , dis[0]/dis[1])


class map():
    def __init__(self):
        pass


# Part 1
# Result:



# print("Part 1:", )


# Part 2
# Result:

# print("Part 2:", )
