#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from computer import Computer, load_memory


# Part 1
# Result: 16348437
print("Part 1. Enter '1'")
mem = load_memory()

comp1 = Computer(mem)
comp1.exec_program()  # Enter '1'


# Part 2
# Result: 6959377
print("\nPart 2. Enter '5'")
mem = load_memory()

comp2 = Computer(mem)
comp2.exec_program()  # Enter '5'
