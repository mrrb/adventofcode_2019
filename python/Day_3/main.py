#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from copy import deepcopy as dc


file_obj = open("input", 'r')
wires = [[[j[0].lower(), int(j[1:])] for j in i.split(',')] for i in file_obj.readlines()]
file_obj.close()


def parse_step(step):
    x, y = 0, 0
    if step[0] == 'r':
        x += step[1]
    elif step[0] == 'l':
        x -= step[1]
    elif step[0] == 'u':
        y += step[1]
    elif step[0] == 'd':
        y -= step[1]
    else:
        raise BaseException(f"Not valid direction {step[0]}.")
    return x, y

def get_paths(wire):
    last = [0, 0]
    paths = [dc(last)]
    for i in wire:
        x_gen, y_gen = parse_step(i)
        last[0] += x_gen
        last[1] += y_gen
        paths.append(dc(last))
    return paths

def get_end_point(wire):
    x, y = 0, 0
    for i in wire:
        x_gen, y_gen = parse_step(i)
        x += x_gen
        y += y_gen
    return x, y

def vector_type(vector):
    Vo, Ve = vector[0], vector[1]
    if Vo[0] == Ve[0] and \
       Vo[1] != Ve[1]:
        return 'v'
    elif Vo[0] != Ve[0] and \
         Vo[1] == Ve[1]:
        return 'h'
    else:
        raise BaseException(f"Vectors out of scope. Neither \"horizontal\" (h) nor \"vertical\" (v).")

def total_len(path):
    total = 0
    for i in path:
        total += i[1]
    return total

def dist(A, B):
    return sum([abs(i) for i in [A[0]-B[0], A[1]-B[1]]])

def det(vec1, vec2):
    return vec1[0]*vec2[1] - vec1[1]*vec2[0]

def get_vector_intersection(vector1, vector2):
    A, B = vector1[0], vector1[1]
    C, D = vector2[0], vector2[1]
    AB = [B[0]-A[0], B[1]-A[1]]
    CD = [D[0]-C[0], D[1]-C[1]]

    # https://en.wikipedia.org/wiki/Intersection_(Euclidean_geometry)
    # x(s) = A[0] + s*(B[0]-A[0]); y(s) = A[1] + s*(B[1]-A[1])
    # x(t) = C[0] + t*(D[0]-C[0]); y(t) = C[1] + t*(D[1]-C[1])
    # ...
    # s*(B[0]-A[0]) - t*(D[0]-C[0]) = C[0]-A[0]
    # s*(B[1]-A[1]) - t*(D[1]-C[1]) = C[1]-A[1]
    # D = det((B[0]-A[0], A[0]-B[0]),
    #         (D[1]-C[1], A[1]-B[1]))
    # s = Ds / D
    # t = Dt / D
    try:
        Db = det((B[0]-A[0], -D[0]+C[0]),
                 (B[1]-A[1], -D[1]+C[1]))
        Ds = det((C[0]-A[0], -D[0]+C[0]),
                 (C[1]-A[1], -D[1]+C[1]))
        Dt = det((B[0]-A[0], C[0]-A[0]),
                 (B[1]-A[1], C[1]-A[1]))
        s, t = Ds/Db, Dt/Db
    
        if (s >= 0 and s <= 1) and (t >= 0 and t <= 1):
            x, y = A[0] + s*(B[0]-A[0]), A[1] + s*(B[1]-A[1])
            return [x, y, dist(A, [x,y]), dist(C, [x,y])]
    except:
        pass

    return []

def get_cross_locations(wire1, wire2):
    paths_wire1 = get_paths(wire1)
    paths_wire2 = get_paths(wire2)

    # A sweepline algorithm should be used for better performance
    cross_points = []
    for index1 in range(len(paths_wire1)-1):
        vector1 = [paths_wire1[index1], paths_wire1[index1+1]]
        for index2 in range(len(paths_wire2)-1):
            vector2 = [paths_wire2[index2], paths_wire2[index2+1]]
            cross = get_vector_intersection(vector1, vector2)
            if cross:
                cross[2] += total_len(wire1[:index1])
                cross[3] += total_len(wire2[:index2])
                cross_points.append(cross)
    # for vector1 in zip(paths_wire1[:-1], paths_wire1[1:]):
    #     for vector2 in zip(paths_wire2[:-1], paths_wire2[1:]):
    #         cross = get_vector_intersection(vector1, vector2)
    #         if cross:
    #             cross_points.append(cross)
    return cross_points


# Info
ep1, len1 = get_end_point(wires[0]), total_len(wires[0])
dist1 = dist([0,0], ep1)
print(f"Wire 1. End={ep1}. Dist_end={dist1}. Len={len1}.")

ep2, len2 = get_end_point(wires[1]), total_len(wires[1])
dist2 = dist([0,0], ep2)
print(f"Wire 2. End={ep2}. Dist_end={dist2}. Len={len2}.")

# Init
cross_locations = get_cross_locations(wires[0], wires[1])
dists = []
lens = []
for index, value in enumerate(cross_locations):
    total_len = value[2] + value[3]
    value = dist((0,0), value)
    if value <= 0:
        continue
    dists.append(value)
    lens.append(total_len)

# Part 1
# Result: 870
print("Part 1:", min(dists))

# Part 2
# Result: 13698
print("Part 2:", min(lens))
