#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from computer import Computer, load_memory


# Part 1
# Result: 2316632620
mem = load_memory("input")
comp1 = Computer(mem, verbose=False, input_register=[1])

comp1.exec_program()

print("Part 1:", comp1.output_register[0])


# Part 2
# Result: 78869
comp2 = Computer(mem, verbose=False, input_register=[2])

comp2.exec_program()

print("Part 2:", comp2.output_register[0])
