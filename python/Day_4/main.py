#!/usr/bin/env python3
# -*- coding: utf-8 -*-


file_obj = open("input", 'r')
pass_range = [int(i) for i in file_obj.readline().split('-')]
file_obj.close()


# Part 1
# Result: 960
pass_list, pass_cnt = [], 0
for num in range(pass_range[0], pass_range[1]+1):
    num_str = str(num)
    num_lst = [int(i) for i in num_str]

    # Check decrease
    dcr = False
    for i in list(zip(num_lst[:-1], num_lst[1:])):
        if i[1] < i[0]:
            dcr = True
            break
    if dcr:
        continue

    # Check pair
    pair = False
    for i in list(zip(num_lst[:-1], num_lst[1:])):
        if i[0] == i[1]:
            pair = True
            break
    if pair:
        pass_list.append(num)
        pass_cnt += 1

print("Part 1:", pass_cnt)

# Part 2
# Result: 626
def chk_group(num_lst):
    pass

pass_list2, pass_cnt2 = [], 0
for num in pass_list:
    num_str = str(num)
    num_lst = [int(i) for i in num_str]

    # Check group
    # 1. Get groups in list
    # 2. Check size
    groups = []
    grp_num = -1
    for i in list(zip(num_lst[:-1], num_lst[1:])):
        if i[1] == grp_num:
            groups[-1][1] += 1
        elif i[0] == i[1]:
            groups.append([i[1], 2])
            grp_num = i[1]
        else:
            grp_num = -1

    grp_counts = [i[1] for i in groups]
    if 2 in grp_counts:
        pass_cnt2 += 1
        pass_list2.append(num)

print("Part 2:", pass_cnt2)
