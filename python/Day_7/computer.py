
def load_memory(filename="input"):
    file_obj = open(filename, 'r')
    memory = {}
    for index, value in enumerate(file_obj.read().split(',')):
        memory[index] = int(value)
    file_obj.close()
    return memory


class Computer():
    def __init__(self, memory, input_register=None, pause_on_input=False, verbose=False):
        self.memory = memory
        self.pc = 0  # Program Counter
        self.op_map = {1: [3, self._op_add],
                       2: [3, self._op_mul],
                       3: [1, self._op_input],
                       4: [1, self._op_output],
                       5: [2, self._op_jit],
                       6: [2, self._op_jif],
                       7: [3, self._op_lt],
                       8: [3, self._op_eq],
                       99: [0, self._op_finish]}

        self.input_register = input_register
        self.output_register = []
        self.verbose = bool(verbose)
        self.pause_on_input = pause_on_input
        self.is_paused = False

    def _value_from_type(self, param):
        if param['type'] == 'value':
            return param['value']
        else:
            return self.memory[param['value']]

    def _op_add(self, params):
        p0 = self._value_from_type(params[0])
        p1 = self._value_from_type(params[1])
        self.memory[params[2]['value']] = p0 + p1

        # print("ADD")
        return True

    def _op_mul(self, params):
        p0 = self._value_from_type(params[0])
        p1 = self._value_from_type(params[1])
        self.memory[params[2]['value']] = p0 * p1

        # print("MUL")
        return True

    def _op_input(self, params):
        if self.input_register:
            val = int(self.input_register.pop(0))
        else:
            if self.pause_on_input:
                self.is_paused = True
                return False
            else:
                val = int(input("IN  > "))
        self.memory[params[0]['value']] = val

        return True

    def _op_output(self, params):
        val = self.memory[params[0]['value']]
        self.output_register.append(val)
        if self.verbose:
            print(f"OUT > {val}")

        return True

    def _op_jit(self, params):
        """jump-if-true"""
        if self._value_from_type(params[0]):
            self.pc = self._value_from_type(params[1])

        return True

    def _op_jif(self, params):
        """jump-if-false"""
        if not self._value_from_type(params[0]):
            self.pc = self._value_from_type(params[1])

        return True

    def _op_lt(self, params):
        """less than"""
        p0 = self._value_from_type(params[0])
        p1 = self._value_from_type(params[1])

        out = 0
        if p0 < p1:
            out = 1
        self.memory[params[2]['value']] = out

        return True

    def _op_eq(self, params):
        """equal"""
        p0 = self._value_from_type(params[0])
        p1 = self._value_from_type(params[1])

        out = 0
        if p0 == p1:
            out = 1
        self.memory[params[2]['value']] = out

        return True

    def _op_finish(self, _):
        # print("FINISH")
        return False

    @staticmethod
    def parse_instruction(ins):
        upper = ins
        params = []
        while True:
            lower = upper % 10
            upper = int(upper / 10)
            params.append(lower)

            if upper == 0:
                break

        return int(''.join(map(str, params[0:2][::-1]))), params[2:]

    def gen_parameters(self, modes, count):
        params = []
        mem = self.memory
        for i in range(count):
            try:
                mode = modes[i]
            except:
                mode = 0

            val_mem = mem[self.pc]
            if mode == 0:
                val = {'type': 'address', 'value': val_mem}
            elif mode == 1:
                val = {'type': 'value', 'value': val_mem}
            else:
                raise BaseException(f"Invalid operation mode {mode}.")

            self.pc += 1
            params.append(val)

        return params

    def exec_program(self):
        self.is_paused = False
        while True:
            pc_start = self.pc
            instruction = self.memory[self.pc]
            op_code, params_mode = self.parse_instruction(instruction)

            op_data = self.op_map.get(op_code)
            if not op_data:
                raise BaseException(
                    f"Invalid OpCode {op_code} at position {self.pc}.")

            self.pc += 1
            par_count, op_func = op_data
            params = self.gen_parameters(params_mode, par_count)
            if callable(op_func):
                if not op_func(params):
                    if self.is_paused:
                        self.pc = pc_start
                    break
