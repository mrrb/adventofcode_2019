#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from copy import deepcopy as dc

from computer import Computer, load_memory


def permutations(data):
    if not data:
        return []
    if len(data) == 1:
        return [data]

    out = []
    for i in range(len(data)):
        pivot = data[i]
        temp_data = data[:i] + data[i+1:]

        for j in permutations(temp_data):
            out.append([pivot] + j)
    
    return out


# Part 1
# Result: 45730
mem = load_memory()

def check_permutation(per):
    out = {'permutation': per}
    last_amp_output = 0
    for comp_num in range(5):
        comp = Computer(dc(mem), input_register=[
                        per[comp_num], last_amp_output])
        comp.exec_program()

        last_amp_output = comp.output_register[0]
        out[f"comp_{comp_num}_out"] = last_amp_output

    return out

out = {}
for per in permutations(list(range(0, 5))):
    out[str(per)] = check_permutation(per)

higher = {'val':0, 'per': None}
for key, value in out.items():
    if value["comp_4_out"] > higher['val']:
        higher['per'], higher['val'] = key, value["comp_4_out"]

print("Part 1:", higher['val'])


# Part 2
# Result: 5406484
mem = load_memory()

out = []
for per in permutations(list(range(5, 10))):
    comps = []
    for comp_num in per:
        comps.append(Computer(dc(mem), input_register=[comp_num], verbose=False, pause_on_input=True))
        comps[-1].exec_program()

    last_amp_output = 0
    while comps[-1].is_paused:
        for comp in comps:
            comp.input_register.append(last_amp_output)
            comp.exec_program()
            last_amp_output = comp.output_register[-1]

    out.append(last_amp_output)
out.sort()

print("Part 2:", out[-1])

