#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from copy import deepcopy as dc


file_obj = open('input', 'r')
data = file_obj.readline().rstrip('\n')
file_obj.close()


def split_in_blocks(data, n_blocks=None, block_len=None):
    if n_blocks:
        block_len = int(len(data)/n_blocks)
    elif block_len:
        n_blocks = int(len(data)/block_len)
    else:
        n_blocks, block_len = 0, 0

    out = []
    for i in range(n_blocks):
        out.append(data[i*block_len:(i+1)*block_len])

    return out


class Image():
    def __init__(self, raw_data, width=25, height=6):
        self.width, self.height = width, height
        self.raw_data = raw_data
        self.layers = None

        self.render_data = None

    def gen_raw_layers(self):
        data = self.raw_data
        w, h = self.width, self.height

        layer_len = w*h
        n_layers = int(len(data)/layer_len)

        return split_in_blocks(data, n_blocks=n_layers)

    def parse_data(self):
        self.layers = {}
        raw_layers = self.gen_raw_layers()

        width = self.width
        for n_layer, layer in enumerate(raw_layers):
            self.layers[n_layer] = {'layer_data': layer,
                                    'layer': split_in_blocks(layer, block_len=width)}

    @staticmethod
    def merge_lines(line1, line2):
        line_out = dc(line1)

        for column in range(len(line_out)): 
            if not line_out[column] in '01':
                line_out = line_out[:column] + \
                    line2[column] + line_out[column+1:]

        return line_out

    def render(self, render_type=None, render_char="+"):
        if not self.layers:
            self.parse_data()

        self.render_data = {}

        data = []
        for line in range(self.height):
            out_line_data = '2'*self.width

            for layer in self.layers.values():
                layer_line_data = layer['layer'][line]

                out_line_data = self.merge_lines(
                    out_line_data, layer_line_data)
                
            data.append(out_line_data)

        if render_type == "number":
            pass
        elif render_type == "color":
            new_data = []
            for line in data:
                temp_line = line.replace(
                    '0', "\033[7m" + "\033[92m" + render_char + "\033[0m")
                temp_line = temp_line.replace(
                    '1', "\033[7m" + "\033[93m" + render_char + "\033[0m")

                new_data.append(temp_line)
            
            data = new_data
        else:
            pass

        self.render_data = {'data': ''.join(data),
                            'render': data}

    def print(self):
        if not self.render_data:
            self.render()

        for line in self.render_data['render']:
            print(line)


# Part 1
# Result: 2125
img = Image(data)
img.parse_data()

min_count = {'layer': -1, 'count': 999}
for n_layer, layer in img.layers.items():
    cnt = layer['layer_data'].count('0')
    if cnt < min_count['count']:
        min_count = {'layer': n_layer, 'count': cnt}

layer_data = img.layers[min_count['layer']]['layer_data']
print("Part 1:", layer_data.count('1')*layer_data.count('2'))


# Part 2
# Result: JYZHF
img.render(render_type="color", render_char=" ")

print("Part 2:")
img.print()
